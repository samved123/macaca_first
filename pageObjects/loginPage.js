module.exports={
    Elements:{
        loginButtonCss:'[href="/s/login?locale=en"]',
        emailByCss :'#user_email',
        passwordByCss:'#user_password',
        log:'[value="Log in"]',
        value:'[data-link-name="Starter"]'
    }
}