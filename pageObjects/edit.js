module.exports={
    Elements:{
        // tourByCss : "div.inner-dialog  div.panel.panel1.active div.next a.s-btn.small.dark-gray"
    settings:'div.top-menu div.purple',
    domains:'div.page-settings-menu.s-dialog-menu li.domains-item',
    domain_name:'div.permalink-container div.permalink-inner input',
    update:'div.s-btn.permalink-update-btn',
    close:'div.page-settings-tab.s-dialog-tab > header > span',
    publish:'a.page.publish-button',
    check:'a.change-url-button'
    }
    
    }