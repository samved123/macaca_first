'use strict';

const {
  assert
} = require('chai');
const wd = require('macaca-wd');
require('./wd-extend')(wd, false);
const loginPage=require('../pageObjects/loginPage.js')
const dashboard=require('../pageObjects/dashboard.js')
const edit=require('../pageObjects/edit.js')
const testData=require('../testdata/logindata.json')
const inputData=require('../testdata/input.json')

var browser ="firefox";
browser = browser.toLowerCase();
const initialURL = 'https://www.qa.strikingly.com/';
describe('macaca-test/desktop-browser-sample.test.js', function() {
  this.timeout(5 * 60 * 1000);

  var driver = wd.promiseChainRemote({
    host: 'localhost',
    port: process.env.MACACA_SERVER_PORT || 3456
  });

  before(() => {
    return driver
      .init({
        platformName: 'desktop',
        browserName: 'chrome',
      })
  });

  describe('LOGIN', function() {
    it('#1 should works with web', function() {
      return driver
        .get(initialURL)
        .elementByCssSelector(loginPage.Elements.loginButtonCss)
        .click()
        .elementByCssSelector(loginPage.Elements.emailByCss)
        .click()
        .sendKeys(testData.login.email)
        .elementByCssSelector(loginPage.Elements.passwordByCss)
    
        .sendKeys(testData.login.passwrd)
        .elementByCssSelector(loginPage.Elements.log)
        .click()
        .elementByCssSelector(loginPage.Elements.value)
        .text()
        .then(value=>{
          console.log("value of text : "+value)
          assert.equal(value.toLowerCase(),"limited","FAIL: does not match")
        });

    });

    
        it('#1 should works with web', function() {
          return driver
        
        .elementByCssSelector(dashboard.Elements.name)
        .text()
        .then(value=>{
          console.log("value of text : "+value)
           assert.equal(value,"PRICING & PLANS","FAIL:  does not match")
          });
            

         
        
        


        
    });
    
        it('#1 should works with web', function() {
          return driver
            
        //   .elementByCssSelector(edit.Elements.tourByCss)
        //   .click()
        .elementByCssSelector(dashboard.Elements.edit)
        .click()
        .elementByCssSelector(edit.Elements.settings)
        .click()
        .elementByCssSelector(edit.Elements.domains)
        .click()
        
        .elementByCssSelector(edit.Elements.domain_name)
        .click()
        .keys('\uE009'+ 'a')
        .clear()
        .sleep(2000)
        .sendKeys(inputData.info.new_name)
        
        .elementByCssSelector(edit.Elements.update)
        .sleep(2000)
        .click()
        .elementByCssSelector(edit.Elements.close)
        .click()
        })
        
        it('#1 should works with web', function() {
          return driver
        .elementByCssSelector(edit.Elements.publish)
        .click()
        .sleep(2000)
        .acceptAlert()
        .sleep(2000)
        .elementByCssSelector(edit.Elements.check).click()
        .text()
        .then(value =>{
         console.log("value",value)
         assert.equal(value,"Change URL",value)
         if( value =="Change URL")
         {
           console.log("change of url is possible")

         }
        else{
           console.log("sorry")
        }
        })
        


        
    });



})
})